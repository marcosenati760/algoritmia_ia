package aplicaciones;

import algoritmogenetico.IHM;
import algoritmogenetico.Individuo;
import algoritmogenetico.Parametros;
import algoritmogenetico.ProcesoEvolutivo;

// Clase lanza las diferentes aplicaciones
public class Aplicacion implements IHM {
    public static void main(String[] args) {
        Aplicacion app = new Aplicacion();
        app.Run();
    }

    public void Run() {
        // Resolución del viajante de comercio
        // Parametros
        Parametros.tasaCrossover = 0.0;
        Parametros.tasaMutacion = 0.3;
        Parametros.tasaAgregaGen = 0.0;
        Parametros.tasaEliminaGen = 0.0;
        Parametros.minFitness = 2579;
        // Ejecución
        ProcesoEvolutivo sist = new ProcesoEvolutivo(this, "PVC");
        Syst.Run();
        
        // Resolución del laberinto
        // Parametros
        Parametros.tasaCrossover = 0.6;
        Parametros.tasaMutacion = 0.1;
        Parametros.tasaAgregaGen = 0.8;
        Parametros.tasaEliminaGen = 0.1;
        Parametros.minFitness = 0;
        Parametros.numMaxGeneraciones = 300;
        // Ejecución
        ProcesoEvolutivo sist2 = new ProcesoEvolutivo(this, "Lab");
        syst2.Run();
    }
    
    @Override
    public void MostrarMejorIndividuo(Individuo ind, int generation) {
        System.out.println(generation + " -> " + ind.toString());
    }
}
