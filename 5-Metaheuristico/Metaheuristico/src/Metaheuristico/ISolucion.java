package Metaheuristico;

// Solución genérica al problema
public interface ISolucion {
    double getValor();
}
